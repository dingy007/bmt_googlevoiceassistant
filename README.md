### Welcome to Google Voice Assistant workshop


- Slides:
    - [Slides in Google Drive](https://docs.google.com/presentation/d/10rT6OF12WfgF-4pYUiAJTfUzpYBcCtlWvMsTKfP1LlQ/edit?usp=sharing)

### WeatherBot Repo
- Backend:
	- Nodejs
        - [https://gitlab.com/balaprasanna/weatherbot_nodejs](https://gitlab.com/balaprasanna/weatherbot_nodejs)
    - Python
        - [https://gitlab.com/balaprasanna/weatherbot_python](https://gitlab.com/balaprasanna/weatherbot_python)


### How to Run a Backend in Herokuo
1. Make sure you have heroku cli installed
2. Make sure you have an heroku account
3. Make sure you have git installed in your machine

## HIGH LEVEL STEPS FOR DEPLOYING TO HEROKU
- clone of the backend repo
- cd inside the repo
- heroku login
- heroku apps:create
- git push heroku master
	
## Steps with example
- clone of the backend repo
	- `git clone <URL>`
- cd inside the repo
	- `cd <dir>`
- heroku login
	- `heroku login  !!You have to enter your username & password`
- heroku apps:create
	- ```
		You will see something like 
		(master)$ heroku apps:create
		Creating app... done, ⬢ <...>
	  ```
	  
- git push heroku master
	```
	git push -u heroku master
	Enumerating objects: 8, done.
	Counting objects: 100% (8/8), done.
	Delta compression using up to 4 threads
	Compressing objects: 100% (6/6), done.
	Writing objects: 100% (8/8), 21.72 KiB | 3.62 MiB/s, done.
	Total 8 (delta 0), reused 0 (delta 0)
	remote: Compressing source files... done.
	remote: Building source:
	remote: 
	remote: -----> Node.js app detected
	remote:        
	remote: -----> Creating runtime environment
	remote:        
	remote:        NPM_CONFIG_LOGLEVEL=error
	remote:        NODE_ENV=production
	remote:        NODE_MODULES_CACHE=true
	remote:        NODE_VERBOSE=false
	remote:        
	remote: -----> Installing binaries
	remote:        engines.node (package.json):  unspecified
	remote:        engines.npm (package.json):   unspecified (use default)
	remote:        
	remote:        Resolving node version 8.x...
	remote:        Downloading and installing node 8.12.0...
	remote:        Using default npm version: 6.4.1
	remote:        
	remote: -----> Building dependencies
	remote:        Installing node modules (package.json + package-lock)
	remote:        
	remote:        > grpc@1.7.3 install /tmp/build_284ddd20937663920a665c931c6a6f50/node_modules/grpc
	remote:        > node-pre-gyp install --fallback-to-build --library=static_library
	remote:        
	remote:        [grpc] Success: "/tmp/build_284ddd20937663920a665c931c6a6f50/node_modules/grpc/src/node/extension_binary/node-v57-linux-x64-glibc/grpc_node.node" is installed via remote
	remote:        
	remote:        > protobufjs@6.8.8 postinstall /tmp/build_284ddd20937663920a665c931c6a6f50/node_modules/protobufjs
	remote:        > node scripts/postinstall
	remote:        
	remote:        added 349 packages from 341 contributors and audited 945 packages in 11.948s
	remote:        found 12 vulnerabilities (2 low, 9 moderate, 1 high)
	remote:          run `npm audit fix` to fix them, or `npm audit` for details
	remote:        
	remote: -----> Caching build
	remote:        - node_modules
	remote:        
	remote: -----> Pruning devDependencies
	remote:        audited 945 packages in 2.746s
	remote:        found 12 vulnerabilities (2 low, 9 moderate, 1 high)
	remote:          run `npm audit fix` to fix them, or `npm audit` for details
	remote:        
	remote: -----> Build succeeded!
	remote: -----> Discovering process types
	remote:        Procfile declares types     -> (none)
	remote:        Default types for buildpack -> web
	remote: 
	remote: -----> Compressing...
	remote:        Done: 37.8M
	remote: -----> Launching...
	remote:        Released v3
	remote:        https://secure-fjord-66976.herokuapp.com/ deployed to Heroku
	remote: 
	remote: Verifying deploy... done.
	To https://git.heroku.com/secure-fjord-66976.git
	 * [new branch]      master -> master
	Branch 'master' set up to track remote branch 'master' from 'heroku'.
		```

*REF*

More Reference:
### CryptoBot
- Codes:
    - Nodejs
        - [https://gitlab.com/balaprasanna/cryptobot_nodejs](https://gitlab.com/balaprasanna/cryptobot_nodejs)
    - Python
        - [https://gitlab.com/balaprasanna/cryptobot_python](https://gitlab.com/balaprasanna/cryptobot_python)